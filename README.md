Retrieve the informations about a query on clinicatrials.gov

API: https://clinicaltrials.gov/api/gui/ref/api_urls

ex de molécules
- imatinib
- tivozanib


**Python API:** en fait il y a même une API python (ne marche qu'avec python2): https://pypi.org/project/clinical_trials/

# Installation

```bash
pip install clinical_trials

python
```


# Data retrieval

```python
from clinical_trials import Trials
t = Trials()
res = t.search("imatinib")
res['search_results']['count']
# '736'

# retrieve the first 3 studies
res = t.search("imatinib", count=3)
res['search_results']['count']
# '736'
len(res['research_results']['clinical_study'])
# '3'

# retrieve the first 1000 studies (even if there are only 736)
res = t.search("imatinib", count=1000)
res['search_results']['count']
# '736'
len(res['research_results']['clinical_study'])
# '736'

# retrieve all the studies
res = t.search("imatinib")
nbStudies = res['search_results']['count']
res = t.search("imatinib", count=nbStudies)


# Download
with open('result-imatinib.zip', w) as resultFile:
    resultFile.write(t.download('imatinib', count=nbStudies))

zipFile = t.download('imatinib', count=nbStudies)
```


